#!/bin/sh

# Adapted from https://github.com/Prodeko/parsedmarc

set -e

envsubst </etc/parsedmarc/parsedmarc.ini.env >/etc/parsedmarc.ini

exec parsedmarc -c /etc/parsedmarc.ini
