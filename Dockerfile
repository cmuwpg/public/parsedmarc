FROM python:3.11.9-slim-bookworm

ENV PARSEDMARC_VERSION=8.11.0 \
    DEBIAN_FRONTEND=noninteractive

RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y patch gettext-base gcc libxml2-dev libxslt-dev libz-dev libemail-outlook-message-perl \
    && pip install -U --no-cache-dir parsedmarc==$PARSEDMARC_VERSION \
    && rm -rf /root/.cache/ \
    && apt-get purge -y gcc \
    && apt-get autoremove -y \
    && apt-get clean \
    && rm -rf /var/lib/{apt,dpkg}/

COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

ENTRYPOINT /entrypoint.sh
